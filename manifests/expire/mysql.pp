class dovecot::expire::mysql {

  file { 'dovecot-dict-expire.conf':
    source => [ "puppet:///modules/site_dovecot/expire/${fqdn}/mysql-dict-expire.conf",
                "puppet:///modules/site_dovecot/expire/mysql-dict-expire.conf",
                "puppet:///modules/dovecot/expire/${operatingsystem}/mysql-dict-expire.conf",
                "puppet:///modules/dovecot/expire/mysql-dict-expire.conf" ],
    path => $operatingsystem ? {
      'debian' => '/etc/dovecot/dovecot-dict-expire.conf',
      default => '/etc/dovecot-dict-expire.conf'
    },
    require => $version ? {
      2 => Package['dovecot-mysql'],
      default => Package['dovecot'],
    },
    notify => Service['dovecot'],
    owner => root, group => 0, mode => 0600;
  }

}
