class dovecot::base {
  file{'/etc/dovecot.conf':
    source  => [ "puppet:///modules/site_dovecot/config/${::fqdn}/dovecot.conf",
                 "puppet:///modules/site_dovecot/config/${::dovecot::type}/dovecot.conf",
                 'puppet:///modules/site_dovecot/config/dovecot.conf',
                 "puppet:///modules/dovecot/config/${::operatingsystem}/dovecot.conf",
                 'puppet:///modules/dovecot/config/dovecot.conf' ],
    require => Package['dovecot'],
    notify => Service['dovecot'],
    owner   => root,
    group   => mail,
    mode    => '0644';
  }

  file { 'dovecot_config_dir':
    ensure => directory,
    path    => '/etc/dovecot/conf.d',
    require => Package['dovecot'],
    owner   => dovecot,
    group   => 0,
    mode    => '0755';
  }

  if !$dovecot::use_syslog {
    file{
      '/var/log/dovecot':
        ensure  => directory,
        require => Package['dovecot'],
        before  => Service['dovecot'],
        owner   => dovecot,
        group   => $dovecot::shared_group,
        mode    => '0660';
      [ '/var/log/dovecot/error.log',
        '/var/log/dovecot/infos.log' ]:
        require => Package['dovecot'],
        before  => Service['dovecot'],
        owner   => root,
        group   => $dovecot::shared_group,
        mode    => '0660';
    }

    include dovecot::logrotate
  }

  file {
    '/var/log/dovecot':
      ensure  => directory,
      require => Package['dovecot'],
      before => Service['dovecot'],
      owner   => dovecot,
      group   => dovecot,
      mode    => '0750';

    [ '/var/log/dovecot/error.log',
      '/var/log/dovecot/dovecot.log' ]:
        require => Package['dovecot'],
        before  => Service['dovecot'],
        owner   => root,
        group   => dovecot,
        mode    => '0660';
  }

  package { 'dovecot':
    ensure => installed,
    alias  => 'dovecot'
  }

  service{'dovecot':
    ensure => running,
    enable => true,
  }
}
