class dovecot::expire ( $type = 'sqlite', $mail_location = '', $dirs = '', $days = '' ) {

  file{'/etc/cron.daily/dovecot-expire':
    owner => root, group => 0, mode => 0755;
  }
  
  if $type == 'legacy' or $type == 'mixed' {
    case $mail_location {
      '': { fail("Need to set \$dovecot_mail_location on $fqdn!") }
    }
    case $dirs {
      '': { $dirs = 'Trash\|Junk' }
    }
    case $days {
      '': { $days = '14' }
    }
    File['/etc/cron.daily/dovecot-expire']{
      content => "find ${mail_location} -regex '.*/\\.\\(${dirs}\\)\\(/.*\\)?\\/\\(cur\\|new\\)/.*' -type f -ctime +${days} -delete\n"
    }
  } else {
    # dovecot version 1 way
    if $version != 2 {
      case $operatingsystem {
        debian: {
          augeas { "expire_cron":
            context => "/files/etc/default/dovecot/rule",
            changes => [ 'set /files/etc/default/dovecot/EXPIRE_CRON \'"daily"\'' ],
          }
        }
        default: {
    File['/etc/cron.daily/dovecot-expire']{
      content => "dovecot --exec-mail ext /usr/libexec/dovecot/expire-tool.sh\n"
    }
  }
      }
    } else {
      # dovecot version 2 way (no mail_location, dirs need to be space separated variables and expire script runs doveadm expunge)
      # problem with this method is that it doesn't allow for different times for different mailboxes
      case $dirs {
        '': { $dirs = 'Trash Junk' }
      }
      case $days {
        '': { $days = '14' }
      }
      File['/etc/cron.daily/dovecot-expire']{
        content => "#!/bin/sh\n\n dirs='${dirs}'\nfor mailbox in \$dirs; do doveadm expunge -A mailbox \$mailbox savedbefore ${days}d; done\n"
      }
    }
  }

  if $type != 'legacy' and $type != 'mysql' and $version != 2 {
    file{'/etc/dovecot-expire.conf':
      source => [ "puppet:///modules/site_dovecot/expire/${fqdn}/dovecot-expire.conf",
                  "puppet:///modules/site_dovecot/expire/dovecot-expire.conf",
                  "puppet:///modules/dovecot/expire/${operatingsystem}/dovecot-expire.conf",
                  "puppet:///modules/dovecot/expire/dovecot-expire.conf" ],
      require => Package['dovecot'],
      notify => Service['dovecot'],
      owner => root, group => 0, mode => 0600;
    }

    file{'/usr/libexec/dovecot/expire-tool.sh':
      source => "puppet:///modules/dovecot/expire/expire-tool.sh",
      owner => root, group => 0, mode => 0700;
    }
  }

  case $type {
    'legacy': { info("no need to include anything for legacy mode") }
    'mixed': { include ::dovecot::expire::sqlite }
    'mysql': { include ::dovecot::expire::mysql }
    default: { include ::dovecot::expire::sqlite }
  }
}
