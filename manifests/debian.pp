# debian-specific package-names and config file paths
class dovecot::debian inherits dovecot::base {

  Package['dovecot'] { name => 'dovecot-common' }

  package { ['dovecot-imapd', 'dovecot-pop3d' ]:
    ensure => installed
  }

  File['/etc/dovecot.conf'] { path => '/etc/dovecot/dovecot.conf' }

}

