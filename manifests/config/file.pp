define dovecot::config::file (
  $ensure = present,
  $source = 'absent',
  $content = 'absent',
  $destination = 'absent',
  $mode = 'absent',
  $owner = 'absent',
  $group = 'absent'
)
{

  # the default destination is 'absent', so if the user doesn't specify a
  # destination, then we use the following defaults. If different systems
  # implement different locations, we can trigger here off of operatingsystem
  # and change the 'dovecot_config_dir' path in base.pp to just be /etc/dovecot
  $real_destination = $destination ? {
    'absent' => "/etc/dovecot/conf.d/${name}",
    default => $destination
  }

  $real_mode = $mode ? {
    'absent' => 0640,
    default => $mode
  }

  $real_owner = $owner ? {
    'absent' => root,
    default => $owner
  }

  $real_group = $group ? {
    'absent' => 0,
    default => $group
  }

  # the $name variable is set to dovecot_${name}, but the actual filename will
  # be set to $name
  file { "dovecot_${name}":
    ensure => $ensure,
    path => $real_destination,
    notify => Service[dovecot],
    owner => $real_owner, group => $real_group, mode => $real_mode;
  }

  # the $content variable is 'absent' by default, so if the user doesn't
  # specify anything for $content, then the following will be used, searching
  # from the first source line until a file is found that matches. We use the
  # standard search prioritizing the site_dovecot module first
  case $content {
    'absent': {
      $real_source = $source ? {
        'absent' => [
                     "puppet:///modules/site_dovecot/config/${fqdn}/${name}",
                     "puppet:///modules/site_dovecot/config/${operatingsystem}/${lsbdistcodename}/${name}",
                     "puppet:///modules/site_dovecot/config/${operatingsystem}/${name}",
                     "puppet:///modules/site_dovecot/config/${name}",
                     "puppet:///modules/dovecot/config/${operatingsystem}/${lsbdistcodename}/${name}",
                     "puppet:///modules/dovecot/config/${operatingsystem}/${name}",
                     "puppet:///modules/dovecot/config/${name}"
                     ],
        default => "puppet:///${source}",
      }
      File["dovecot_${name}"]{
        source => $real_source,
      }
    }
    default: {
      File["dovecot_${name}"]{
        content => $content,
      }
    }
  }
  File["dovecot_${name}"]{
    require => Package[dovecot],
  }
}
