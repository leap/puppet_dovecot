class dovecot::sieve {

  include ::dovecot

  package{'dovecot-sieve':
    ensure => installed,
    before => Service['dovecot'],
  }

  $sieve_location = $operatingsystem ? {
    debian => '/var/lib/dovecot/sieve',
    default => '/var/lib/dovecot-sieve'
  }

  file {
    $sieve_location:
      ensure => directory,
      owner => root, group => mail, mode => 0775;
 
    "${sieve_location}/global":
    ensure => directory,
    recurse => true,
    purge => true,
    force => true,
    notify => Exec['compile_global_sieve'],
    owner => root, group => root, mode => 0644;

    "${sieve_location}/default.sieve":
      source => [ "puppet:///modules/site_dovecot/sieve/${fqdn}/default.sieve",
                  "puppet:///modules/site_dovecot/sieve/default.sieve",
                  "puppet:///modules/dovecot/sieve/${operatingsystem}/default.sieve",
                "puppet:///modules/dovecot/sieve/default.sieve" ],
    notify => Exec['compile_default_sieve'],
      owner => root, group => root, mode => 0644;

    # this is for sequential sieve scripts, configured in 90-sieve.conf as:
    #  sieve_before = /var/lib/dovecot/sieve/default.sieve
    "${sieve_location}/before.sieve":
      source => [ "puppet:///modules/site_dovecot/sieve/${fqdn}/before.sieve",
                  "puppet:///modules/site_dovecot/sieve/before.sieve",
                  "puppet:///modules/dovecot/sieve/${operatingsystem}/before.sieve",
                  "puppet:///modules/dovecot/sieve/before.sieve" ],
      notify => Exec['compile_before_sieve'],
      owner => root, group => root, mode => 0644;

    # this is for sequential sieve scripts, configured in 90-sieve.conf as:
    #  sieve_after = /var/lib/dovecot/sieve/after.sieve
    "${sieve_location}/after.sieve":
      source => [ "puppet:///modules/site_dovecot/sieve/${fqdn}/after.sieve",
                  "puppet:///modules/site_dovecot/sieve/after.sieve",
                  "puppet:///modules/dovecot/sieve/${operatingsystem}/after.sieve",
                  "puppet:///modules/dovecot/sieve/after.sieve" ],
      notify => Exec['compile_after_sieve'],
    owner => root, group => root, mode => 0644;
  }

  exec {
    'compile_default_sieve':
      command => "sievec ${sieve_location}/default.sieve",
      creates => "${sieve_location}/default.svbin",
      require => File["${sieve_location}/default.sieve"];

    'compile_before_sieve':
      command => "sievec ${sieve_location}/before.sieve",
      creates => "${sieve_location}/before.svbin",
      require => File["${sieve_location}/before.sieve"];
    
    'compile_after_sieve':
      command => "sievec ${sieve_location}/after.sieve",
      creates => "${sieve_location}/after.svbin",
      require => File["${sieve_location}/after.sieve"];

    'compile_global_sieve':
      command => "sievec ${sieve_location}/global/",
      refreshonly => true;
  }
}
