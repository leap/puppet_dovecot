class dovecot::sql::pgsql {

  if $version == 2 {
    package { 'dovecot-pgsql':
      ensure => installed,
      before => File['/etc/dovecot-sql.conf'],
    }
  }
}
